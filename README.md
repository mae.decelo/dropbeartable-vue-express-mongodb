# DropBearTable - Vue Express MongoDB

The final implementation of the DropBearTable web application using the Vue framework for the client-side, and an Express/MongoDB stack on the server-side. 


# Client-Side Implementation

This implementation has been tested for use in a Docker container. Ensure that Docker
and Docker Compose are installed on your local machine. To begin using the DropBearTable
client and server in a Docker container, open a terminal in the dropbeartable_mevn 
directory and run the command:

```
docker compose up --build -d
``` 

Once you are finished using the application, enter the following command to close 
the Docker containers:

```
docker compose down -v
```


### Importing prefilled MongoDB collections


***!! IMPORTANT***: **The server will not run correctly if the `restaurants` and `bookings`
collections are not imported to MongoDB first.** 


The database is implemented using MongoDB running in the Docker container. A set of
prefilled collections have been provided in the `dropbeartable_mevn/server/resources`
directory to be imported to MongoDB Compass to test the capabilities of the server.
To set this up:
- Access the MongoDB database on MongoDB Compass by entering the connection string  
  `mongodb://admin:password@localhost:27017/`
- Add a database called `test`
- Within the `test` database, create collections named according to the collections'
  names provided in the 'resources' directory:
    - restaurants
    - bookings
- Import the collections in the 'resources' directory to the collections in the `test`
  database according to their names
- You can begin using the server once all collections are imported into the correct
  collections in the `test` database

### Running Nightwatch tests

To run the example test in `client/tests/e2e/home.test.js`, you are required to install
the dependencies on your local machine by running `npm install` and to have Firefox installed
in the default location.

The test can be run by entering `npm run test:e2e`. The results will be printed onto the 
terminal, and a screenshot can be found in `client/screenshots`.


## Usage


### Home page

The home page can be accessed on `http://localhost:8080/`. This page shows the buttons that 
links to the different restaurants on DropBearTable within the card components in the middle 
section of the page. 

The navigation bar shows buttons to open the login window, to scroll to the 'Contact Us' form,
and to go to the page containing the d3 graph. 


**PLEASE NOTE**: To get access to the 'My Bookings' button that links to the page containing 
all existing bookings, you are required to log in first. 


### Logging in

To log into DropBearTable, click the login button in the navigation bar, which will open the login
form. Enter `name@example.com` in the email address input and `password` in the password field. 

The hourglass processing icon will appear for 3 seconds, after which the login button will be 
replaced by the 'My Bookings' button.


### My bookings - reading all existing bookings

The bookings page on `http://localhost:8080/bookings` displays all the existing bookings in the 
database. You can perform actions on this page that includes searching for a booking by its ID, 
editing a booking, or deleting a booking.


- **Search for a booking**: to search for a booking, enter the `Booking ID` of the booking entry
you are after
  - The search bar will validate if the search input is a valid ObjectId
  - If the search bar is clicked with an empty search input or with characters that are not a valid
  ObjectId, the error message underneath the search bar that says "Please enter a valid booking ID."
  will appear


###### **EDITING A BOOKING**
- To edit a booking, click the edit button that appears underneath the booking you are after
  - This will open the 'Edit Booking' window that contains a dropdown list of parameters that can be
  edited: the number of guests, the guest name, the guest's mobile number, and additional requests
  - The text input next to it allows the user to input the new value for the parameter chosen 
  - The form will validate the input to ensure that the inputs are appropriate for the parameter chosen
  or that the text input is not empty
  - The `Name` input must be at least 2 characters in length and cannot contain special characters or 
  numbers
  - The `Number of Guests` input must be a number between 1 and 12
  - The `Mobile Number` input must be a valid 10-digit Australian mobile phone number, which starts with
  '04'
  - The `Additional Requests` input has a character limit of 100
  - After clicking the 'Confirm Edit' button with a valid input, the hourglass icon will appear for 5 
  seconds before showing the response window that says if the edit request was successful or not
  - After the user clicks the close button of the response window, the page will refresh and show the 
  changes to the booking

###### **DELETING A BOOKING**
- To delete a booking, click the delete button underneath the booking to be deleted
  - This will open the 'Delete Booking' window that contains the 'Confirm Delete' button 
  - Clicking the 'Confirm Delete' button sends the `DELETE` request to the server and removes the booking
  from the database 
  - After clicking the 'Confirm Delete' button, the hourglass icon will appear for 5 seconds and is then
  replaced by the response window that states if the `DELETE` request was successful
  - After clicking the close button of the response window, the page is refreshed and shows that the booking
  has been removed if the request was successful


### Restaurant page

Each individual restaurant's page can be accessed from the home page on `http://localhost:8080/` by clicking
the restaurant's booking button within the restaurant card component in the middle section of the page. 


- **Image carousel**: within the restaurant page, users can view the image carousel featuring images 
from each restaurant by clicking the `<` and `>` buttons
  - The image carousel also automatically switches the images every 2 seconds 


###### **CREATING A BOOKING:**

- **Search bar**: the search bar features a date, time, and number of guests 
input
  - The date input will show dates 24 hours from the current time and up to two weeks from
  the current date
  - The dropdown list for the booking times feature the available booking times for the individual
  restaurant as specified in the information retrieved from the server
  - The dropdown list for the number of guests features the numbers 1 to 12


- **Confirmation window**: after choosing the input parameters and clicking the 'Check Availability' button, the 
confirmation window will appear with the details chosen by the user and a form to enter the guest's name, mobile 
number and additional notes
  - The form validates that the `Name` input is at least 2 characters and only contains valid characters, the 
  `Mobile Number` input is a valid Australian mobile number, and that the `Additional Information` input is 
  below 100 characters
  - The form will also not accept an empty `Name` or `Mobile Number` field, but the `Additional Information`
  input is optional
  - After the user clicks the 'Submit' button, the hourglass icon will appear for 2 seconds, after which the
  response window appears that tells the user the booking request has been successfully submitted or the error
  response if the request is unsuccessful

###### **SERVER SENT EVENTS**

- **Notification**: If the request was successfully submitted, a red dot will appear over the notification 
icon located on the lower right of the page
  - This means that the notification has successfully received the server sent event confirming the 
  creation of a new reservation
  - If the user clicks on the notification button, the notification window will appear that shows the
  booking has been confirmed, as well as the new booking's ID, and the red dot will disappear from the 
  notification button


### Graph

The graph on `http://localhost:8080/graph` can be accessed through the 'Graph' button located on the 
navigation bar. This page displays a line plot of the number of bookings for each date stored in the database.


### Webpack

The minified and compiled CSS and JavaScript files are found in the `/client/dist/` directory.

-----------------------------------------------------------------------------------------



# Server-Side Implementation

### Swagger documentation

To view the swagger documentation, open [http://localhost:3000/docs/](http://localhost:3000/docs/)
in a web browser. 

## Usage

### Retrieving all bookings 

To retrieve all bookings stored in the database, run the following curl command in the terminal. 
This sends a GET request to the server, which returns all the bookings in JSON format:

```curl
curl --location --request GET 'localhost:3000/bookings' \
| json_pp 
```

### Retrieving a single booking by id

***!! IMPORTANT***: Please note that the restaurant ID is different to a booking ID. Ensure
that you are using the booking ID when looking for a booking.

To retrieve a single booking by its id, run the following command. This sends a GET
request to the server, which returns the data associated with the id specified. Replace
the `<OBJECT ID>` with the actual object id of the booking that you are after:

```curl
curl --location --request GET 'localhost:3000/bookings/<OBJECT ID>' \
--header 'Content-Type: application/json' \
| json_pp 
```

**Example**:
- Restaurant 3 is prefilled with a booking that has the object id `6126d76ff28ed1004afb3e43`
for the guest `"Jane Doe"`. Run the following command to retrieve the booking information:

```curl
curl --location --request GET 'localhost:3000/bookings/6126d76ff28ed1004afb3e43' \
--header 'Content-Type: application/json' \
| json_pp
```

### Creating a new booking

The following curl command sends a POST request to the server, along with the details 
of the booking to be stored: 

```curl
curl --location --request POST 'localhost:3000/bookings' \
--header 'Content-Type: application/json' \
--data-raw '{
    "restaurant": {
        "id": "<RESTAURANT ID>",
        "date": "<DATE OF BOOKING>",
        "time": "<TIME OF BOOKING>",
        "pax": <NUMBER OF GUESTS>
    },
    "person": {
        "name": "<NAME OF GUEST>",
        "mobile": "<GUEST'S MOBILE NUMBER>",
        "notes": "<ADDITIONAL INFORMATION>"
    }
}' \
| json_pp
```

**Booking details validation**: 
- The `<RESTAURANT ID>` must be a String and a valid ID of a restaurant stored in the 
DropBearTable server.
  - ***!! IMPORTANT***: Please note that the restaurant ID is different to a booking ID.
  Ensure that you are using a valid restaurant ID when creating a POST request. 
  - Restaurant 1 has the ID `6106565cddbcc8004ef91c8a`
  - Restaurant 2 has the ID `61065623ddbcc8004ef91c87`
  - Restaurant 3 has the ID `610655d1ddbcc8004ef91c84`
  - If the id is not stored in the DropBearTable server, the server will return an error message
- The `<DATE OF BOOKING>` must be a String and a valid date in the format YYYY-MM-DD. The
server will send an error message if the date is from before the current date or if the date 
is more than two weeks from the current date
- The `<TIME OF BOOKING>` must be a String in a 24-hour format. Each restaurant accepts
bookings in 30-minute intervals and have unique opening and closing times:
  - Restaurant 1 accepts bookings from 12:00 to 23:30
  - Restaurant 2 accepts bookings from 9:00 to 22:30
  - Restaurant 3 accepts bookings from 10:00 to 21:30
  - If the request includes a time outside these hours, or if the time entered is not in a
  24-hour format in 30-minute intervals, the server will return an error
- The `<NUMBER OF GUESTS>` must be an integer, with a minimum of 1 guest and a maximum of
12 guests per booking. Each restaurant has a unique max capacity for every hour that they
are open:
  - Restaurant 1 has a max capacity of 30 guests per hour
  - Restaurant 2 has a max capacity of 20 guests per hour 
  - Restaurant 3 has a max capacity of 15 guests per hour
  - If the restaurant already has existing bookings stored and the requested guest number
  added is over the maximum capacity, the server will return an error
- The `<NAME OF GUEST>` must be at least 2 alphabetical characters 
- The `<GUEST'S MOBILE NUMBER>` must be a valid 10-digit Australian mobile phone number 
i.e., begins with 04
- The `<ADDITIONAL INFORMATION>` is an optional field that has a maximum character length 
of 100 

Notes: 
- The `status` is automatically added by the server for every new POST request and is in the 
state "Processing". After 5 seconds, the server automatically changes the `status` to "Confirmed".
It can then be updated by users using the PUT request. 
- The restaurant name is retrieved and entered in successful bookings by the server and cannot be
manually edited by the user.
- If the same booking details are sent to the server, an error message will be sent back since it 
is a duplicate of an already existing booking.

**Example**:
- To create a booking for `"John Doe"` for Restaurant 1, type the following command in the terminal:

```curl
curl --location --request POST 'localhost:3000/bookings' \
--header 'Content-Type: application/json' \
--data-raw '{
    "restaurant": {
        "id": "6106565cddbcc8004ef91c8a",
        "date": "2021-08-15",
        "time": "18:30",
        "pax": 10
    },
    "person": {
        "name": "John Doe",
        "mobile": "0487654321",
        "notes": ""
    }
}' \
| json_pp
```

- After the response is sent back, check the console on the web browser to see the server sent 
event

- Restaurant 3 is prefilled with a reservation for `"Jane Doe"` for 10 guests on 2021-08-16 at 
21:30. If we try to create a booking for 10 more people for the same date and hourly time slot, 
we will get an error message since the max capacity for Restaurant 3 for any given hour is 
15 people. You can try this with the command below:

```curl
curl --location --request POST 'localhost:3000/bookings' \
--header 'Content-Type: application/json' \
--data-raw '{
    "restaurant": {
        "id": "610655d1ddbcc8004ef91c84",
        "date": "2021-08-16",
        "time": "21:00",
        "pax": 10
    },
    "person": {
        "name": "John Doe",
        "mobile": "0487654321",
        "notes": ""
    }
}' \
| json_pp
```

- Restaurant 3 also opens at 10:00 and closes at 21:30. If we try to add a booking for 9:00, 
the server will return an error message:

```curl
curl --location --request POST 'localhost:3000/bookings' \
--header 'Content-Type: application/json' \
--data-raw '{
    "restaurant": {
        "id": "610655d1ddbcc8004ef91c84",
        "date": "2021-08-16",
        "time": "9:00",
        "pax": 10
    },
    "person": {
        "name": "Jane Doe",
        "mobile": "0404123456",
        "notes": ""
    }
}' \
| json_pp
```

### Updating a field of an existing booking by id

***!! IMPORTANT***: Please note that the restaurant ID is different to a booking ID. Ensure
that you are using the booking ID when looking for a booking.

A single existing booking can be retrieved and have a one or multiple of its fields updated 
per request using the curl command:

```curl
curl --location --request PUT 'localhost:3000/bookings/<OBJECT ID>' \
--header 'Content-Type: application/json' \
--data-raw '{
    "status": "<STATUS VALUE>",
    "restaurant": {
        "pax": <NUMBER OF GUESTS>
    },
    "person": {
        "name": "<NAME OF GUEST>",
        "mobile": "<MOBILE NUMBER>",
        "notes": "<ADDITIONAL INFORMATION>"
    }
}' \
| json_pp 
```

- Replace `<OBJECT ID>` with the id of an existing booking
- Each request can update one or two of the "status", "restaurant", or "person" objects or all 
of them at once:
  - The valid "status" values are `Confirmed`, `Cancelled`, `Completed`, or `Processing`
    (these values are case-sensitive)
  - The `<NUMBER OF GUESTS>` must be an integer from 1 to 12 and is validated against the 
  max capacity of the restaurant with already existing bookings
  - The `<NAME OF GUEST>` must use alphabetical letters and has a minimum length of 2
  - The `<MOBILE NUMBER>` must be a valid 10-digit Australian mobile number
  - The `<ADDITION INFORMATION>` has a limit of 100 characters

**Example**:
- Restaurant 3 is prefilled with a booking for `Jane Doe` that has the object id
`6126d76ff28ed1004afb3e43`. Run the following command to update the guest's name:

```curl
curl --location --request PUT 'localhost:3000/bookings/6126d76ff28ed1004afb3e43' \
--header 'Content-Type: application/json' \
--data-raw '{
    "person": {
        "name": "Bob"
    }
}' \
| json_pp
```

- Multiple fields can also be updated in the same request. Run the following command
to update the booking status to `Cancelled` and the guest's mobile number to `0498765432`:

```curl
curl --location --request PUT 'localhost:3000/bookings/6126d76ff28ed1004afb3e43' \
--header 'Content-Type: application/json' \
--data-raw '{
    "status": "Cancelled",
    "person": {
        "mobile": "0498765432"
    }
}' \
| json_pp
```

### Deleting an existing booking by id

***!! IMPORTANT***: Please note that the restaurant ID is different to a booking ID. Ensure
that you are using the booking ID when looking for a booking.

To delete an existing booking by id, use the following curl command. Replace the `<OBJECT ID>` 
with the actual object id of the booking that you are after: 

```curl
curl --location --request DELETE 'localhost:3000/bookings/<OBJECT ID>' \
| json_pp
```

**Example**:
- Restaurant 3 is prefilled with a booking that has an object id `6126d76ff28ed1004afb3e43`. 
Run the following command to delete this booking:

```curl
curl --location --request DELETE 'localhost:3000/bookings/6126d76ff28ed1004afb3e43' \
| json_pp
```

### Running jest tests

The tests in the /server/tests directory can be run by running the following command within
the Docker container. To open the Docker container commandline, enter the command in your
local machine's terminal: 

```bash
docker exec -it dropbeartable_mevn_dbt-server_1 bash
```

To run the test within the container, run the command:

```bash
npm run test
```

Exit the Docker container commandline by entering `exit`.


