const bookings = require("../models/bookings").bookings;
const bookingValidator = require("../services/bookingsValidation");
const { setTimeout } = require('timers');
const { validationResult } = require("express-validator");
const {isValidObjectId} = require("mongoose");

let clients = [];

/**
 * Function to call the write method of the res object and send out the event.
 * @param newBooking - the JSON of the data to send with the event
 */
const sendEvents = (newBooking) => {
    clients.forEach(client =>
        client.res.write(`event:bookingAdded\ndata: ${JSON.stringify(newBooking)}\n\n`))
}

/**
 * Function to allow clients to connect to the server.
 * @param req - request object
 * @param res - response object
 */
exports.bookingAddedNotification = (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
    res.setHeader('Connection', 'keep-alive')
    res.setHeader('Content-Type', 'text/event-stream')
    res.setHeader('Cache-Control', 'no-cache')
    console.log(res)
    const clientId = Date.now();
    const newClient = {
        id: clientId,
        res
    };
    clients.push(newClient);
    res.on('close', () => console.log(`connection closed!`));
}

/**
 * Function to find all bookings.
 * @param req - the request received
 * @param res - the response object sent
 * @returns {Promise<void>} - the bookings found for the requested name and mobile number
 */
exports.find_all_bookings = async (req, res) => {
    await bookings.find()
        .then(async bookings => {
            if (!bookings[0]) {
                let err = {
                    "msg": "Error! Cannot find any bookings."
                };
                return await res.status(404).json({errors: [err]});
            }

            let results_array = [];
            for (let i = 0; i < bookings.length; i++){
                results_array.push(bookings[i])
            }

            if (results_array.length !== 0) {
                return await res.status(200).json(results_array);
            }
        })
        .catch(error => {
            console.log(error);
            let err = {
                "msg": "Error! Cannot retrieve booking data."
            };
            res.status(400).json({errors: [err]});
        })
};

/**
 * Function to find a single booking by its Object ID
 * @param req - the request received
 * @param res - the booking found with the requested ID
 */
exports.find_booking_by_id = async (req, res) => {
    // Filter results by booking id specified in the route
    const filter = { _id: req.params._id };

    // Check if the id specified in the path is a valid Object ID
    if (!isValidObjectId(req.params._id)) {
        let err = {
            "msg": "Error! Booking ID specified is invalid.",
            "values": [req.params._id],
            "location": "path",
            "parameters": ["_id"]
        };
        return await res.status(400).json({errors: [err]})
    }

    return await bookings.findById(filter)
        .then(async booking => {
            if (booking === null) {
                let err = {
                    "msg": "Error! Cannot find a booking with the _id specified.",
                    "values": [req.params._id],
                    "location": "path",
                    "parameters": ["_id"]
                };
                return await res.status(404).json({errors:[err]});
            }

            return await res.status(200).json(booking);
        })
        .catch(error => {
            console.log(error);
            let err = {
                "msg": "Error! Cannot find booking information."
            };
            res.status(400).json({errors: [err]});
        })
};

/**
 * Function to create and add a new booking to the database.
 * @param req - the request containing the booking details
 * @param res - the error or success message. If a success message is sent, the json data will also be sent
 * @returns {Promise<*>} - the results of the async validation functions
 */
exports.add_new_booking = async (req, res) => {
    // Check if the restaurant Object ID specified is valid
    if (!isValidObjectId(req.body.restaurant.id)) {
        let err = {
            "msg": "Error! Restaurant ID specified is invalid.",
            "values": [req.body.restaurant.id],
            "location": "path",
            "parameters": ["restaurant.id"]
        };
        return res.status(400).json({errors: [err]})
    }

    // Get the booking collection model for the requested restaurant id
    const restaurant_name = await bookingValidator.getRestaurantName(req.body.restaurant.id);

    // Check if restaurant requested is a valid restaurant
    if (!restaurant_name) {
        let err = {
            "msg": "Error! Restaurant requested does not exist on DropBearTable.",
            "values": [req.body.restaurant.id],
            "location": "body",
            "parameters": ["restaurant.id"]
        };
        return res.status(400).json({errors: [err]})
    }

    // Check if values entered in fields are valid according to validation settings
    let errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json( { errors: errors.array() } )
    }

    // Check if requested time is a valid option
    if(!await bookingValidator.isValidTimeOption(req)) {
        let err = {
            "msg": "Time entered is not valid for the restaurant requested. Time must also be " +
                "a String in 24-hour format.",
            "values": [req.body.restaurant.time],
            "location": "body",
            "parameters": ["restaurant.time"]
        };
        return res.status(400).json({errors: [err]})
    }

    // Check if the request is a duplicate of an existing booking
    if(await bookingValidator.isDuplicate(req)) {
        let err = {
            "msg": "Error! Request is a duplicate of an already existing booking.",
            "values": [req.body.restaurant.id, req.body.restaurant.date, req.body.restaurant.time,
                       req.body.restaurant.pax, req.body.person.name, req.body.person.mobile],
            "location": "body",
            "parameters": ["restaurant.id", "restaurant.date", "restaurant.time", "restaurant.pax",
                           "person.name", "person.mobile"]
        };
        return res.status(400).json({errors: [err]})
    }

    // Check if the number of guests in existing bookings and the request goes over the max capacity
    if(await bookingValidator.isOverCapacity(req.body.restaurant.id, req.body.restaurant.date,
        req.body.restaurant.time, req.body.restaurant.pax)) {
        let err = {
            "msg": "Error! The requested restaurant is fully booked for this time slot. Cannot create booking.",
            "values": [req.body.restaurant.id, req.body.restaurant.date, req.body.restaurant.time, req.body.restaurant.pax],
            "location": "body",
            "parameters": ["restaurant.id", "restaurant.date", "restaurant.time", "restaurant.pax"]
        };
        return res.status(400).json({errors: [err]})
    }

    // Create a new booking Schema from request body
    let requested_bookingData = {
        "status": "Processing",
        "restaurant": {
            "id": req.body.restaurant.id,
            "name": restaurant_name,
            "date": new Date(req.body.restaurant.date),
            "time": req.body.restaurant.time,
            "pax": parseInt(req.body.restaurant.pax)
        },
        "person": {
          "name": req.body.person.name,
          "mobile": req.body.person.mobile,
          "notes": req.body.person.notes
        }
    };

    // Create a new booking model for the restaurant requested
    const save_bookingData = await new bookings(requested_bookingData);

    // Save request body to mongoDB then send the response
    await save_bookingData.save()
        .then( async () => {
            let response = {
                "msg": "Thank you, " + req.body.person.name + "! Your booking request has been submitted." +
                  "\n\nPlease wait for the notification after your booking has been confirmed.",
                "data": save_bookingData
            }
            res.status(201).json({messages: [response]});

            // Set booking status to confirmed after 5 seconds
            await setTimeout(async () => {
                bookings.updateOne({_id: save_bookingData._id}, {$set: {status: "Confirmed"}})
                    .then(() => console.log("Updated booking status to 'Confirmed'."))
                    .catch( err => console.log(err))

                let response = {
                    "msg": "Hi, " + req.body.person.name + "! Your booking request has been confirmed!" +
                      "\n\nYour booking ID is " + save_bookingData._id + ".",
                    "data": save_bookingData
                }

                // Server sent event to the client at localhost:8080
                return sendEvents(response);

            }, 5000);
        })
        .catch( error => {
            console.log(error)
            let err = {
                "msg": "Error! Booking cannot be saved."
            }
            res.status(400).json({errors: [err]});
        })
};

/**
 * Function to update a single or multiple parameter(s) for a booking that is found using its Object ID
 * @param req - the request sent containing the parameter to be changed and the value to change to
 * @param res - the success or error message
 * @returns {Promise<void>} - the response to the request
 */
exports.update_booking = async (req, res) => {
    // Check if the id specified in the path is a valid Object ID
    if (!isValidObjectId(req.params._id)) {
        let err = {
            "msg": "Error! Booking ID specified is invalid.",
            "values": [req.params._id],
            "location": "path",
            "parameters": ["_id"]
        };
        return res.status(400).json({errors: [err]})
    }

    const filter = {_id: req.params._id};  // The booking id
    const keys = Object.keys(req.body);  // The keys present in the request body 'status', 'restaurant' or 'person'
    const updated_info = {};  // Object to add details of updated information

    // Validate if the request body is not empty
    if (Object.keys(req.body).length === 0) {
        let err = {
            "msg": "Error! Request body cannot be empty.",
            "location": "body",
            "parameters": ["req.body"]
        };
        return await res.status(400).json({errors:[err]});
    }

    // If the request body contains the restaurant object, get all its keys
    let restaurant_keys;
    if (req.body.restaurant !== undefined) {
        restaurant_keys = Object.keys(req.body.restaurant);
    }

    // If the request body contains the person object, get all its keys
    let person_keys;
    if (req.body.person !== undefined) {
        person_keys = Object.keys(req.body.person);
    }

    // Check if values entered in fields are valid according to validation settings
    let errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json( { errors: errors.array() } )
    }

    // Get the details of the original booking
    let result = await bookings.findById(filter).exec();

    // Check if the id specified is valid
    if (result === null) {
        let err = {
            "msg": "Error! Cannot find a booking with the id specified.",
            "values": [req.params._id],
            "location": "path",
            "parameters": ["_id"]
        };
        return await res.status(404).json({errors:[err]});
    }

    // Error object
    let err = {
        "msg": "Error! Invalid parameter in request.",
        "values": [],
        "location": "body",
        "parameters": []
    };

    for (let i = 0; i < keys.length; i++) {
        // If the request body contains the 'status' object
        if (keys[i] === "status") {
                updated_info[`status`] = req.body.status;
        }

        // If the request body contains the 'restaurant' object
        else if (keys[i] === "restaurant") {
            // Check the keys inside the restaurant object
            for (let j = 0; j < restaurant_keys.length; j++) {
                if (restaurant_keys[j] === "pax") {
                    // Validate if the pax requested will be over the restaurant's max capacity
                    if (await bookingValidator.isOverCapacity(result.restaurant.id, result.restaurant.date,
                        result.restaurant.time, req.body.restaurant.pax)) {
                        let err = {
                            "msg": "Error! Restaurant is fully booked for this time slot. Cannot create booking.",
                            "values": [req.body.restaurant.pax],
                            "location": "body",
                            "parameters": ["restaurant.pax"]
                        };
                        return res.status(400).json({errors: [err]})
                    }
                    // Add the 'restaurant.pax' key and value to the updated_info object
                    updated_info[`restaurant.pax`] = req.body.restaurant.pax;
                }
                // Add error to err object if the restaurant object contains keys other than pax
                else {
                    err.values.push(req.body.restaurant);
                    err.parameters.push("restaurant");
                }
            }
        }
        // If the request body contains the 'person' object
        else if (keys[i] === "person") {
            // For each key inside the 'person' object
            for (let k = 0; k < person_keys.length; k++) {
                if (person_keys[k] === "name") {
                    updated_info[`person.name`] = req.body.person.name;
                } else if (person_keys[k] === "mobile") {
                    updated_info[`person.mobile`] = req.body.person.mobile;
                } else if (person_keys[k] === "notes") {
                    updated_info[`person.notes`] = req.body.person.notes;
                }
                // Add error to err object if other parameters are entered in the person object
                else {
                    err.values.push(req.body.person);
                    err.parameters.push("person");
                }
            }
        }
        // Add error to err object if invalid objects are entered in the request body
        else {
            err.values.push(req.body);
        }
    }

    // Return a 400 error if the err.values is not empty
    if (err.values.length !== 0) {
        return await res.status(400).json({errors: [err]});
    }

    // Update the information
    bookings.updateOne(filter, updated_info, {runValidators: true})
        .then(async () => {
            let updated_booking = await bookings.findById(filter).exec();
            let response = {
                "msg": "Booking successfully updated!",
                "data": updated_booking
            };
            res.status(200).json({messages: [response]});
        })
        .catch(error => {
            console.log(error);
            let err = {
                "msg": "Error! Cannot update booking information."
            };
            res.status(400).json({errors: [err]});
        })
};

/**
 * Function to delete an existing booking that is found using its Object ID
 * @param req - the request containing the Object ID
 * @param res - the error or success message
 * @returns {Promise<void>} - the booking model type of the requested Object
 */
exports.delete_booking = async (req, res) => {
    // Check if the id specified in the path is a valid Object ID
    if (!isValidObjectId(req.params._id)) {
        let err = {
            "msg": "Error! Booking ID specified is invalid.",
            "values": [req.params._id],
            "location": "path",
            "parameters": ["_id"]
        };
        return res.status(400).json({errors: [err]})
    }

    const filter = {_id: req.params._id};

    // Check if the booking exists
    let result = await bookings.findById(filter).exec();

    if (result === null) {
        let err = {
            "msg": "Error! Cannot find a booking with the id specified.",
            "values": [req.params._id],
            "location": "path",
            "parameters": ["_id"]
        };
        return await res.status(404).json({errors:[err]});
    }

    bookings.deleteOne(filter)
        .then(() => {
            let response = {
                "msg": "Booking successfully deleted!"
            };
            res.status(200).json({messages: [response]});
        })
        .catch(error => {
            console.log(error);
            let err = {
                "msg": "Error! Cannot delete booking."
            };
            res.status(400).json({error: [err]});
        })
};