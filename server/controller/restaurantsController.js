const bookingValidator = require("../services/bookingsValidation");
const restaurantsModel = require("../models/restaurants").restaurants;

// FIND ALL EXISTING RESTAURANTS
exports.find_all_restaurants = async (req, res) => {
    await restaurantsModel.find()
        .then(async restaurants => {
            let results_array = [];
            for (let i = 0; i < restaurants.length; i++){
                results_array.push(restaurants[i])
            }
            return await res.json(results_array);
        })
        .catch(error => {
            console.log(error);
            let err = {
                "message": "Error! Cannot retrieve information about all restaurants."
            };
            res.status(400).json({errors: [err]});
        })
};

// FIND RESTAURANT BY ID
exports.find_restaurant_by_id = async (req, res) => {
    // Filter results by booking id specified in the route
    const filter = { _id: req.params.restaurant_id };

    await restaurantsModel.findById(filter)
        .then(async restaurant => {
            await res.json(restaurant)
        })
        .catch(async error => {
            console.log(error);
            let err = {
                "message": "Error! Cannot find restaurant information."
            };
            await res.status(400).json({errors: [err]});
        })
};

// ADD NEW RESTAURANT TO COLLECTION
exports.add_new_restaurant = (req, res) => {
    const restaurantData = new restaurantsModel(req.body);

    restaurantData.save()
        .then( () => {
            let response = {
                "message": "Details for " + req.body.information.name + " have been saved."
            }
            res.status(201).json({messages: [response]});
        })
        .catch( error => {
            console.log(error)
            let err = {
                "message": "Error! Restaurant cannot be saved."
            }
            res.status(400).json({errors: [err]});
        })
}

// UPDATE RESTAURANT BY ID
exports.update_restaurant = (req, res) => {
    const filter = { _id: req.params.restaurant_id };  // Get restaurant ID
    const docKey = Object.keys(req.body)[0];  // Get main key i.e. "information" or "web_parameter"
    const subDoc = Object.values(req.body)[0];  // Get key/value pair of sub doc in request body
    const subDocKey = Object.keys(subDoc)[0];  // Get key of sub doc to be updated
    const subDocValue = Object.values(subDoc)[0]; // Get the value to be updated
    const key = docKey + "." + subDocKey;  // Concatenate nested keys
    const updateInfo = { $set:  { [key] : subDocValue } }; // Set the nested key/value pair to be updated

    // Update only one field per request
    if(!bookingValidator.isSingleParameter(req)) {
        let err = {
            "message": "Error! Please update only one field per request."
        };
        res.status(400).json({errors: [err]});
    }

    const find_restaurant = restaurantsModel.updateOne(filter, updateInfo);

    Promise.all([find_restaurant])
        .then(() => {
            let response = {
                "message": "Restaurant information updated! The restaurant " + subDocKey + " has been changed to '"
                    + subDocValue + "'.",
            };
            res.json({messages: [response]});
        })
        .catch(error => {
            console.log(error);
            let err = {
                "message": "Error! Cannot update restaurant information."
            };
            res.status(400).json({errors: [err]});
        })

}

// REMOVE RESTAURANT BY ID
exports.delete_restaurant = (req, res) => {
    const find_restaurant = restaurantsModel.deleteOne({_id: req.params.restaurant_id});

    Promise.all([find_restaurant])
        .then(() => {
            let response = {
                "message": "Restaurant successfully deleted!"
            };
            res.json({messages: [response]});
        })
        .catch( error => {
            console.log(error);
            let err = {
                "message": "Error! Cannot delete restaurant information."
            };
            res.status(400).json({error: [err]});
        })
};