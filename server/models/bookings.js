const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create mongoose booking Schema
const BookingSchema = new Schema({
    status: String,
    restaurant: {
        id: mongoose.Types.ObjectId,
        name: String,
        date: Date,
        time: String,
        pax: Number
    },
    person: {
        name: String,
        mobile: String,
        notes: String
    }
});

const Bookings = mongoose.model("Bookings", BookingSchema, "bookings");

module.exports = {
    bookings: Bookings
}