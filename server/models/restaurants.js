const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create mongoose booking Schema
const RestaurantSchema = new Schema({
    information: {
        name: String,
        capacity: Number,
        booking_times: [String],
    },
    website_parameter: {
        detail: String,
        sub_detail: String,
        page_link: String,
        link_name: String,
        img: Array
    }
});

const restaurants = mongoose.model("restaurants", RestaurantSchema, "restaurants");

module.exports = {
    restaurants: restaurants
}
