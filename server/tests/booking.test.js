const restaurant3_Booking = require("../models/bookings").restaurantThreeBookings;

test("Booking model should return correct values specified", () => {
    const bookingInfo = {
        "status": "Processing",
        "restaurant": {
            "id": "610655d1ddbcc8004ef91c84",
            "date": "2021-08-14",
            "time": "13:00",
            "pax": 8
        },
        "person": {
            "name": "Bob",
            "mobile": "0434567890",
            "notes": "Table beside the window please"
        }
    }

    const bookingModel = new restaurant3_Booking(bookingInfo);

    expect(bookingInfo.status).toBe(bookingModel.get("status"));
    expect(bookingInfo.restaurant.id).toBe(bookingModel.get("restaurant.id"));
    expect(bookingInfo.restaurant.date).toBe(bookingModel.get("restaurant.date"));
    expect(bookingInfo.restaurant.time).toBe(bookingModel.get("restaurant.time"));
    expect(bookingInfo.restaurant.pax).toBe(bookingModel.get("restaurant.pax"));
    expect(bookingInfo.person.name).toBe(bookingModel.get("person.name"));
    expect(bookingInfo.person.mobile).toBe(bookingModel.get("person.mobile"));
    expect(bookingInfo.person.notes).toBe(bookingModel.get("person.notes"));
});