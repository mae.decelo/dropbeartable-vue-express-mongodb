const { PORT } = require('./config')
module.exports = {
    options: {
        definition: {
            openapi: '3.0.0',
            info: {
                title: 'DropBearTable Bookings',
                version: '0.0.1',
                description: 'An API for the CRUD functions of DropBearTable.'
            },
        },
        host: `localhost:${PORT}`,
        basePath: '/',
        apis: ['./routes/bookings.js'],
    }
}