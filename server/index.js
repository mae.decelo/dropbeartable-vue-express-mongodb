const features =
    [
        {
            cssId:"checkbox1",
            marks:"5",
            descriptionTrue:"Feature 1 - implemented and functional",
            descriptionFalse:"Feature 1 - not implemented and/or not functional",
            title:"Feature 1 - Implemented and functional (5)",
            mistakes: [
                {
                    cssId: "checkbox1_1",
                    marks: 1,
                    descriptionTrue: "'A' was used",
                    descriptionFalse: "'A' was not used",
                    title: "mistake A"
                },
                {
                    cssId: "checkbox1_2",
                    marks: 1,
                    descriptionTrue: "'B' was used",
                    descriptionFalse: "'B' was not used",
                    title: "mistake B"
                },
                {
                    cssId: "checkbox1_3",
                    marks: 1,
                    descriptionTrue: "'C' was used",
                    descriptionFalse: "'C' was not used",
                    title: "mistake C"
                },
                {
                    cssId: "checkbox1_4",
                    marks: 1,
                    descriptionTrue: "'D' was used",
                    descriptionFalse: "'D' was not used",
                    title: "mistake D"
                },
                {
                    cssId: "checkbox1_5",
                    marks: 1,
                    descriptionTrue: "'E' was used",
                    descriptionFalse: "'E' was not used",
                    title: "mistake E"
                }
            ]
        },
        {
            cssId:"checkbox2",
            marks:"15",
            descriptionTrue:"Feature 2 - implemented and functional",
            descriptionFalse:"Feature 2 - not implemented and/or not functional",
            title:"Feature 2 - Implemented and functional (15)",
            mistakes: [
                {
                    cssId: "checkbox2_1",
                    marks: 5,
                    descriptionTrue: "'A' was used",
                    descriptionFalse: "'A' was not used",
                    title: "A implemented?"
                },
                {
                    cssId: "checkbox2_2",
                    marks: 5,
                    descriptionTrue: "'B' was used",
                    descriptionFalse: "'B' was not used",
                    title: "B implemented?"
                },
                {
                    cssId: "checkbox2_3",
                    marks: 5,
                    descriptionTrue: "'C' was used",
                    descriptionFalse: "'C' was not used",
                    title: "C implemented?"
                }
            ]
        }
    ]

// MONGO DB
const mongoose = require('mongoose');
const { MONGO_IP, MONGO_PORT, MONGO_USERNAME, MONGO_PASSWORD, PORT } = require("./config/config")
const mongoDbUrl = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_IP}:${MONGO_PORT}`;

// EXPRESS
const express = require('express');
const app = express();

// PARSE JSON
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

// ROUTES
const bookingsRouter = require("./routes/bookings");
app.use("/bookings", bookingsRouter);

const restaurantsRouter = require("./routes/restaurants");
app.use("/restaurants", restaurantsRouter);

// SWAGGER
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerOptions = require('./config/swagger');
const swaggerUi = require('swagger-ui-express');

// MONGOOSE FUNCTIONS
mongoose.connect(mongoDbUrl, {useNewUrlParser: true, useUnifiedTopology: true})
    .then( () => {
        console.log("Connected to MongoDB.");
    })
    .catch( error => {
        console.error(error);
        console.error("Error encountered while connecting to MongoDB.");
    });
mongoose.set('useFindAndModify', false);
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to database!'));

// PORT
app.listen(PORT, () => console.log(`Express is listening on port ${PORT}!`))

// SWAGGER
const openApiSpecification = swaggerJSDoc(swaggerOptions.options);
app.use('/docs', swaggerUi.serve, swaggerUi.setup(openApiSpecification));


// TUTORIAL
app.get('/features', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
    res.json(features);
});