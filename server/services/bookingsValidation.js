const bookings = require("../models/bookings").bookings;
const restaurantsList_Information = require("../models/restaurants").restaurants;
const { body } = require("express-validator");

/**
 * Array of times in thirty minute intervals within an array
 * @type {string[][]}
 */
const slots = [["9:00", "9:30"], ["10:00", "10:30"], ["11:00", "11:30"], ["12:00", "12:30"], ["13:00", "13:30"],
    ["14:00", "14:30"], ["15:00", "15:30"], ["16:00", "16:30"], ["17:00", "17:30"], ["18:00", "18:30"],
    ["19:00", "19:30"], ["20:00", "20:30"], ["21:00", "21:30"], ["22:00", "22:30"]];

/**
 * Function to find the max capacity of a restaurant using its name.
 * @returns {Promise<*>} - the capacity of a restaurant
 * @param restaurant_id - the id of a restaurant
 */
const getMaxCapacity = async (restaurant_id) => {
    const restaurant_model = await getRestaurantInformation(restaurant_id);
    return restaurant_model.information.capacity;
}

/**
 * Function to find the booking model of the restaurant by its name.
 * @returns {Promise<*>} - the model of the restaurant
 * @param req_restaurant_id - the id of a restaurant
 */
const getRestaurantInformation = async (req_restaurant_id) => {
    return restaurantsList_Information.findOne({_id: req_restaurant_id})
        .then(result => {
            return result
        })
        .catch(() => {
            return false
        })
}

/**
 * Validation chain to validate the parameters received in the request body for new bookings.
 * @type {ValidationChain[]}
 */
exports.validateNewBookingRequest = [
    // Validate that status is not declared in booking request
    body('status').trim().isEmpty().withMessage('Cannot manually enter booking status through POST requests.'),

    // Validate restaurant data
    body('restaurant.id').trim().isLength({ min: 24, max: 24 }).withMessage('Restaurant id is invalid.'),
    body('name').trim().isEmpty().withMessage('Cannot manually enter restaurant name through POST requests.'),
    body('restaurant.date').trim().isISO8601().withMessage('Booking date must be a valid date in the format ' +
        'YYYY-MM-DD.')
        .custom(date =>{
            let requestedDate = new Date(date);
            let currentDate = new Date();
            let twoWeeksMax = new Date();
            twoWeeksMax.setDate(twoWeeksMax.getDate() + 14);
            if (requestedDate <= currentDate) {
                throw new Error('Date must be valid. Cannot accept dates from the past.');
            } else if (requestedDate > twoWeeksMax) {
                throw new Error('Can only accept bookings up to two weeks from current date.');
            }
            return true;
        }),
    body('restaurant.pax').trim().isInt({ min:1, max:12 }).withMessage('Number of guests for a booking ' +
        'must be between 1 to 12.'),

    // Validate person data
    body('person.name').trim().isLength({ min: 2, max: 20 }).withMessage('Name of person creating a ' +
        'booking must be at least 2 characters.').isAlpha('en-AU', {ignore: ' '}).withMessage('Name of ' +
        'person creating a booking  cannot have characters other than alphabetical letters and spaces.'),
    body('person.mobile').trim().isMobilePhone("en-AU").withMessage('Mobile number must be a valid ' +
        '10-digit Australian mobile number.'),
    body('person.notes').optional({nullable: true, checkFalsy: true}).trim().isLength({ max: 100 })
        .escape().withMessage('Additional information must have a maximum character length of 100.')
]

/**
 * Validation chain to validate the parameters received in the request body for updating existing bookings.
 * @type {ValidationChain[]}
 */
exports.validateUpdateBooking = [
    // Validate status value
    body('status').optional({nullable: true, checkFalsy: true}).custom(status => {
        // Validate the value of 'status'
        if (status !== "Confirmed" && status !== "Processing" && status !== "Cancelled" && status !== "Completed") {
            throw new Error('Invalid value entered for status. Status can only be Confirmed, Processing, ' +
                'Cancelled or Completed');
        }
        return true;
    }),

    // Validate restaurant data
    body('restaurant.pax').optional({nullable: true, checkFalsy: true}).trim()
        .isInt({ min:1, max:12 }).withMessage('Number of guests for a booking must be between 1 to 12.'),

    // Validate person data
    body('person.name').optional({nullable: true, checkFalsy: true}).trim()
        .isLength({ min: 2, max: 20 }).withMessage('Name of person creating a booking must be at least ' +
        '2 characters.').isAlpha('en-AU', {ignore: ' '}).withMessage('Name of person creating a ' +
        'booking  cannot have characters other than alphabetical letters and spaces.'),
    body('person.mobile').optional({nullable: true, checkFalsy: true}).trim().isMobilePhone("en-AU")
        .withMessage('Mobile number must be a valid 10-digit Australian mobile number.'),
    body('person.notes').optional({nullable: true, checkFalsy: true}).trim().isLength({ max: 100 })
        .escape().withMessage('Additional information must have a maximum character length of 100.')
]

/**
 * Function to retrieve the name of a restaurant given the restaurant id.
 * @param restaurant_id - the restaurant id
 * @returns {Promise<*|boolean>} - the restaurant name
 */
exports.getRestaurantName = async (restaurant_id) => {
    const restaurant_info = await getRestaurantInformation(restaurant_id);
    if (restaurant_info !== null) {
        return restaurant_info.information.name
    }
    return false;
}

/**
 * Function to check if the request received has the same details as an existing booking.
 * @param req - the request body
 * @returns {Promise<*>} - the matching existing booking
 */
exports.isDuplicate = async (req) => {
    return await bookings.findOne({
        'restaurant.date': req.body.restaurant.date, 'restaurant.time': req.body.restaurant.time,
        'restaurant.pax': req.body.restaurant.pax, 'person.name': req.body.person.name,
        'person.mobile': req.body.person.mobile
    })
        .then(async result => {
            return await result;
        })
        .catch(error => {
            console.log(error);
        })}

/**
 * Function to check if the requested booking time is a valid option for the restaurant requested.
 * @param req - the request body
 * @returns {Promise<boolean>} - if the requested time is valid
 */
exports.isValidTimeOption = async (req) => {
    const restaurant_info = await getRestaurantInformation(req.body.restaurant.id);
    const restaurant_booking_times = restaurant_info.information.booking_times;
    // Check if requested time matches a valid time in the restaurant information
    let matching_option = [];
    for(let i = 0; i < restaurant_booking_times.length; i++) {
        if(req.body.restaurant.time === restaurant_booking_times[i]){
            matching_option.push(restaurant_booking_times[i])
        }
    }
    return matching_option.length !== 0;
}

/**
 * Function to validate the number of guests requested and the total guests already booked against the
 * restaurant's max capacity.
 * @param restaurant_id - the id of the restaurant
 * @param request_date - the requested date
 * @param request_time - the requested time
 * @param request_pax - the requested number of guests
 * @returns {Promise<boolean>} - if the number of guests is over the max capacity
 */
exports.isOverCapacity = async (restaurant_id, request_date, request_time, request_pax) => {
    let guests = [];
    let sum_of_guests = 0;

    // Get the maximum capacity of a restaurant
    const restaurant_capacity = await getMaxCapacity(restaurant_id);

    let date = new Date(request_date);

    // Array of all bookings from the same date as the request
    let bookingsForDate = await bookings.aggregate([
        {$match: {'restaurant.date': date}}
    ])

    console.log(bookingsForDate[0]);

    for (const slotTime_number of slots) {
        for(let j=0; j < 2; j++) {
            if (slotTime_number[j] === request_time) {
                // For all bookings from the same date, find ones for the same time slot
                bookingsForDate.forEach(bookings_on_day => {
                    slotTime_number.forEach(time_slot => {
                        if (bookings_on_day.restaurant.time === time_slot) {
                            guests.push(bookings_on_day.restaurant.pax);
                        }
                    })
                })
                guests.forEach(key => {
                    sum_of_guests += parseInt(key);
                })
                sum_of_guests += parseInt(request_pax);

                return sum_of_guests > parseInt(restaurant_capacity);
            }
        }
    }
}