const express = require('express');
const router = express.Router();
const bookings_controller = require("../controller/bookingsController");
const bookings_validator = require("../services/bookingsValidation");

/**
 * Route for the events sent to localhost:8080 after successful POST requests.
 */
router.get("/notification", bookings_controller.bookingAddedNotification);

/**
 * @openapi
 * /bookings:
 *  get:
 *    summary: Get all bookings
 *    tags: [Bookings]
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: Returns all bookings
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  _id:
 *                    type: ObjectId
 *                    example: 61079a0dc47c67072073239b
 *                  status:
 *                    type: string
 *                    example: Confirmed
 *                  restaurant:
 *                    type: object
 *                    properties:
 *                      id:
 *                          type: ObjectId
 *                          example: 6106565cddbcc8004ef91c8a
 *                      name:
 *                        type: string
 *                        example: Restaurant 1
 *                      date:
 *                        type: Date
 *                        example: 2021-09-20
 *                      time:
 *                        type: string
 *                        format: time
 *                        example: 12:00
 *                      pax:
 *                        type: Number
 *                        example: 6
 *                  person:
 *                    type: object
 *                    properties:
 *                      name:
 *                        type: string
 *                        example: Jane Doe
 *                      mobile:
 *                        type: string
 *                        example: "0404123456"
 *                      notes:
 *                        type: string
 *                        example: Window seat please.
 *                  __v:
 *                    type: integer
 *                    format: int64
 *      404:
 *        description: Returns an error array
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                errors:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: string
 *                        example: Error! Cannot find any bookings.
 */
router.get("/", bookings_controller.find_all_bookings);

/**
 * @openapi
 * /bookings/{_id}:
 *  get:
 *    summary: Get a booking using its _id
 *    tags: [Bookings]
 *    parameters:
 *      - in: path
 *        name: _id
 *        schema:
 *          type: string
 *        required: true
 *    responses:
 *      200:
 *        description: Return booking with the specified _id
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                _id:
 *                  type: ObjectId
 *                  example: 6109ee916b3b96002000c1af
 *                status:
 *                  type: string
 *                  example: Confirmed
 *                restaurant:
 *                  type: object
 *                  properties:
 *                    id:
 *                      type: ObjectId
 *                      example: 610655d1ddbcc8004ef91c84
 *                    name:
 *                      type: string
 *                      example: Restaurant 3
 *                    date:
 *                      type: Date
 *                      example: 2021-08-16
 *                    time:
 *                      type: string
 *                      format: time
 *                      example: 21:30
 *                    pax:
 *                      type: Number
 *                      example: 8
 *                person:
 *                  type: object
 *                  properties:
 *                    name:
 *                      type: string
 *                      example: Jane Doe
 *                    mobile:
 *                      type: string
 *                      example: "0404123456"
 *                    notes:
 *                      type: string
 *                      example: Window seat please.
 *                __v:
 *                  type: integer
 *                  format: int64
 *      404:
 *        description: Returns an error array
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                errors:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: string
 *                        example: Error! Cannot find a booking with the _id specified.
 *                      values:
 *                        type: [ObjectId]
 *                        example: [61088b47d2bd68003c28e3ac]
 *                      location:
 *                        type: string
 *                        example: path
 *                      parameters:
 *                        type: [string]
 *                        example: [_id]
 */
router.get("/:_id", bookings_controller.find_booking_by_id);

/**
 * @openapi
 * /bookings:
 *  post:
 *    summary: Add a booking to the bookings collection
 *    tags: [Bookings]
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              restaurant:
 *                type: object
 *                properties:
 *                  id:
 *                    type: ObjectId
 *                    example: 61065623ddbcc8004ef91c87
 *                  date:
 *                    type: Date
 *                    example: 2021-08-15
 *                  time:
 *                    type: string
 *                    format: time
 *                    example: 12:00
 *                  pax:
 *                    type: Number
 *                    example: 6
 *              person:
 *                type: object
 *                properties:
 *                  name:
 *                    type: string
 *                    example: Bob
 *                  mobile:
 *                    type: string
 *                    example: "0412345678"
 *                  notes:
 *                    type: string
 *                    example: Window seat please.
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      201:
 *        description: Returns a success message array with the booking information
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                messages:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: string
 *                        example: Thank you, Bob! Your booking has been confirmed.
 *                      data:
 *                        type: object
 *                        properties:
 *                          _id:
 *                            type: ObjectId
 *                            example: 61088b47d2bd68003c28e3ac
 *                          status:
 *                            type: string
 *                            example: Processing
 *                          restaurant:
 *                            type: object
 *                            properties:
 *                              id:
 *                                type: string
 *                                example: 61065623ddbcc8004ef91c87
 *                              name:
 *                                type: string
 *                                example: Restaurant 2
 *                              date:
 *                                type: Date
 *                                example: 2021-09-20
 *                              time:
 *                                type: string
 *                                format: time
 *                                example: 12:00
 *                              pax:
 *                                type: Number
 *                                example: 6
 *                          person:
 *                            type: object
 *                            properties:
 *                              name:
 *                                type: string
 *                                example: Bob
 *                              mobile:
 *                                type: string
 *                                example: "0412345678"
 *                              notes:
 *                                type: string
 *                                example: Window seat please.
 *                          __v:
 *                            type: integer
 *                            format: int64
 *      400:
 *        description: Returns an error array
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                errors:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: string
 *                        example: Error! Restaurant 1 is fully booked for this time slot. Cannot create booking.
 *                      values:
 *                        type: [string]
 *                        example: [Restaurant 1, 2021-09-20, 12:00, 10]
 *                      location:
 *                        type: string
 *                        example: body
 *                      parameters:
 *                        type: [string]
 *                        example: [restaurant.name, restaurant.date, restaurant.time, restaurant.pax]
 */
router.post("/", bookings_validator.validateNewBookingRequest, bookings_controller.add_new_booking);

/**
 * @openapi
 * /bookings/{_id}:
 *  put:
 *    summary: Update a field in a booking using its _id
 *    tags: [Bookings]
 *    parameters:
 *      - in: path
 *        name: _id
 *        schema:
 *          type: string
 *        required: true
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              status:
 *                type: string
 *                example: Cancelled
 *              restaurant:
 *                type: object
 *                properties:
 *                  pax:
 *                    type: Number
 *                    example: 4
 *              person:
 *                type: object
 *                properties:
 *                  name:
 *                    type: string
 *                    example: "Bob"
 *                  mobile:
 *                    type: string
 *                    example: "0487654321"
 *                  notes:
 *                    type: string
 *                    example: Booking for a birthday party.
 *    responses:
 *      200:
 *        description: A success message array
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                messages:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: string
 *                        example: Booking successfully updated!
 *                      data:
 *                        type: object
 *                        properties:
 *                          _id:
 *                            type: ObjectId
 *                            example: 6109ee916b3b96002000c1af
 *                          status:
 *                            type: string
 *                            example: Cancelled
 *                          restaurant:
 *                            type: object
 *                            properties:
 *                              id:
 *                                type: ObjectId
 *                                example: 610655d1ddbcc8004ef91c84
 *                              name:
 *                                type: string
 *                                example: Restaurant 3
 *                              date:
 *                                type: Date
 *                                example: 2021-08-16
 *                              time:
 *                                type: string
 *                                format: time
 *                                example: 21:30
 *                              pax:
 *                                type: Number
 *                                example: 4
 *                          person:
 *                            type: object
 *                            properties:
 *                              name:
 *                                type: string
 *                                example: Bob
 *                              mobile:
 *                                type: string
 *                                example: "0487654321"
 *                              notes:
 *                                type: string
 *                                example: Booking for a birthday party.
 *                          __v:
 *                            type: integer
 *                            format: int64
 *                            example: 0
 *      400:
 *        description: Returns an error array if there are invalid parameters inside the request
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                errors:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: string
 *                        example: Error! Invalid parameter in request.
 *                      values:
 *                        type: array
 *                        items:
 *                          type: string
 *                          example: Restaurant 5
 *                      location:
 *                        type: string
 *                        example: body
 *                      parameters:
 *                        type: array
 *                        items:
 *                          type: string
 *                          example: restaurant.name
 *      404:
 *        description: Returns an error array if the _id is not in the database
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                errors:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: string
 *                        example: Error! Cannot find a booking with the _id specified.
 *                      values:
 *                        type: [ObjectId]
 *                        example: [61088b47d2bd68003c28e3ac]
 *                      location:
 *                        type: string
 *                        example: path
 *                      parameters:
 *                        type: [string]
 *                        example: [_id]
 */
router.put("/:_id",  bookings_validator.validateUpdateBooking, bookings_controller.update_booking);

/**
 * @openapi
 * /bookings/{_id}:
 *  delete:
 *    summary: Delete a booking using its _id
 *    tags: [Bookings]
 *    parameters:
 *      - in: path
 *        name: _id
 *        schema:
 *          type: string
 *        required: true
 *    responses:
 *      200:
 *        description: Delete booking with the specified _id
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                messages:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: string
 *                        example: Booking successfully deleted!
 *      404:
 *        description: Returns an error array
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                errors:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      message:
 *                        type: string
 *                        example: Error! Cannot find a booking with the _id specified.
 *                      values:
 *                        type: [ObjectId]
 *                        example: [61088b47d2bd68003c28e3ac]
 *                      location:
 *                        type: string
 *                        example: path
 *                      parameters:
 *                        type: [string]
 *                        example: [_id]
 */
router.delete("/:_id", bookings_controller.delete_booking);

module.exports = router;