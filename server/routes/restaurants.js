const express = require('express');
const router = express.Router();
const restaurantsController = require("../controller/restaurantsController");

// Bookings routes - specify function from the controller for each endpoint
router.get("/", restaurantsController.find_all_restaurants);
router.get("/:restaurant_id", restaurantsController.find_restaurant_by_id);
router.post("/", restaurantsController.add_new_restaurant);
router.put("/:restaurant_id", restaurantsController.update_restaurant);
router.delete("/:restaurant_id", restaurantsController.delete_restaurant);

module.exports = router;