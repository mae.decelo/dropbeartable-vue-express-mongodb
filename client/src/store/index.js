import { createStore } from 'vuex'

export default createStore({
  state: {
    bookings: null,
    restaurants: null,
    data: [],
    sse: []
  },
  mutations: {
    // BOOKINGS
    setBookings (state, bookings) {
      state.bookings = bookings
    },
    // RESTAURANTS
    setRestaurants (state, restaurants) {
      state.restaurants = restaurants
    },
    // DATA
    setData (state, data) {
      state.data = data
    },
    // SERVER SENT EVENTS
    setSSE (state, sse) {
      state.sse = sse
    }
  },
  actions: {
    // GET ALL BOOKINGS
    getBookings({ commit }) {
      fetch('http://localhost:3000/bookings')
        .then(res => res.json())
        .then(data => {
          let bookingData = data.map(booking => {
            return booking
          })
          commit('setBookings', bookingData);
        })
        .catch(err => console.log(err));
    },

    // GET ALL RESTAURANTS
    async getRestaurants({ commit }) {
      await fetch('http://localhost:3000/restaurants')
        .then(async res => await res.json())
        .then(async data => {
          let restaurantsData = data.map(restaurant => {
            return restaurant
          });
          await commit('setRestaurants', restaurantsData);
        })
        .catch(err => console.error(err))
    },

    // GET STATISTICS DATA
    async getData({commit}) {
      await fetch('http://localhost:3000/bookings')
        .then(async res => await res.json())
        .then(async data => {
          let object = {};
          let array = [];
          let i;

          // Count the number of occurrences of the date in the bookings stored
          for (let i=0; i < data.length; i++) {
            let date = new Date(data[i].restaurant.date);
            object[date] = object[date] || 0;
            object[date]++;
          }

          // Add new entry to array if object with date not in array yet
          for (i in object) {
            if (Object.prototype.hasOwnProperty.call(object, i)) {
              array.push({ date: new Date(i), count: object[i] });
            }
          }

          // Sort objects in array by date ascending
          array.sort(function (a, b) {
            let c = new Date(a.date);
            let d = new Date(b.date);
            return c - d;
          });

          await commit('setData', array);
        })
        .catch(err => console.log(err));
    },

    // GET SERVER SENT EVENTS
    getSSE({commit}) {
      const sse = new EventSource('http://localhost:3000/bookings/notification');
      sse.addEventListener("bookingAdded", (e) => {
        commit('setSSE', JSON.parse(e.data))
      })
    }
  },
  getters: {
    bookings_array: (state) => {
      return state.bookings
    }
  }
})