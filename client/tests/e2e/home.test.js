module.exports = {
  "test bookings search button empty input validation": browser => {
    browser
      .url(`${browser.launchUrl}/bookings`)
      .waitForElementVisible("#submitBookingBtn")
      .click("#submitBookingBtn")
      .waitForElementVisible("#searchInvalid")
      .assert.containsText("#searchInvalid", "Please enter a valid booking ID.")
      .saveScreenshot("./screenshots/search_validation.png")
      .end();
  }
}